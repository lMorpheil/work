active = (x) => {
  let link = document.getElementsByClassName('nav__link')[x];
  link.classList.add('nav__link_active');
  for(i=0; i < 5; i++) {
    if(i===x) continue;
    document.getElementsByClassName('nav__link')[i].classList.remove('nav__link_active');
  }
}
