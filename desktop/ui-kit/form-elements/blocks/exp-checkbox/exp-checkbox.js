showlist = () => {
  let list = document.getElementsByClassName('checkbox__list_exp')[0];
  let arrow = document.getElementsByClassName('exp-checkbox__arrow')[0];

  if (list.style.display === 'none') {
    arrow.style.background = 'no-repeat url("../../desktop/ui-kit/form-elements/blocks/exp-checkbox/exp-checkbox-inactive.svg") center center';
    list.style.display = 'block';
  } else {
      list.style.display = 'none';
      arrow.style.background = 'no-repeat url("../../desktop/ui-kit/form-elements/blocks/exp-checkbox/exp-checkbox-active.svg") center center';
  }
};
