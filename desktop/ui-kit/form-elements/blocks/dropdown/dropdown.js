let plus = function(){
  let quantity = $(this).parent().find('.dropdown__q')[0];
  let sum = Number(quantity.value);
  let btnMinus = $(this).parent().find('.dropdown__btn-minus')[0];
  let drop = $(this).parents('.dropdown__window')[0];
  quantity.value = sum + 1;
  btnMinus.removeAttribute('disabled');
  if ($(this).parents('.dropdown_add')[0] != undefined) result.call(drop);
};

let minus = function(){
  let quantity = $(this).parent().find('.dropdown__q')[0];
  let sum = Number(quantity.value);
  let btnMinus = $(this).parent().find('.dropdown__btn-minus')[0];
  let drop = $(this).parents('.dropdown__window')[0];
  quantity.value = sum - 1;
  if ($(this).parents('.dropdown_add')[0] != undefined) result.call(drop);
  if (Number(quantity.value) === 0) btnMinus.setAttribute('disabled', 'disabled');
};

let result = function(){
  let guest = '', lastNumber;
  let a = Number($(this).parents('.dropdown').find('.dropdown__q')[0].value),
      b = Number($(this).parents('.dropdown').find('.dropdown__q')[1].value),
      c = Number($(this).parents('.dropdown').find('.dropdown__q')[2].value);
  let sum = a + b + c;
  let bedroom, bathroom, bed;
  lastNumber = sum % 10;
  if ($(this).parents('.dropdown_add')[0] != undefined) {
    switch (true) {
      case a === 0: bedroom = ''
        break;
      case a === 1: bedroom = a + " " + 'спальня'
        break;
      case a > 1 && a < 5: bedroom = a + " " + 'спальни'
        break;
      case a > 4 && a < 10: bedroom = a + " " + 'спален'
        break;
    }
    switch (true) {
      case b === 0: bed = ''
        break;
      case b === 1: bed = ", " + b + " " + 'кровать'
        break;
      case b > 1 && b < 5: bed = ", " + b + " " + 'кровати'
        break;
      case b > 4 && b < 10: bed = ", " + b + " " + 'кроватей'
        break;
    }
    switch (true) {
      case c === 0: bathroom = ''
        break;
      case c === 1: bathroom = ", " + c + " " + 'ванная'
        break;
      case c > 1 && c < 5: bathroom = ", " + c + " " + 'ванные'
        break;
      case c > 4 && c < 10: bathroom = ", " + c + " " + 'ванных'
        break;
    }
    $(this).parents('.dropdown').find('.dropdown__input')[0].value = bedroom + bed + bathroom;
  } else {
    switch (true) {
      case sum > 4 && sum < 21: guest = 'гостей'
        break;
      case lastNumber === 1: guest = 'гость'
        break;
      case lastNumber > 1 && lastNumber < 5: guest = 'гостя'
        break;
      case lastNumber > 4 && lastNumber < 10: guest = 'гостей'
        break;
      case lastNumber === 0: guest = 'гостей'
        break;
    }
    $(this).parents('.dropdown').find('.dropdown__input')[0].value = sum + " " + guest;
  }
  if (sum > 0) $(this).parents('.dropdown').find('.dropdown__clear')[0].style.visibility = 'visible';
};

let cleaning = function(){
  let quantity = $(this).parents('.dropdown').find('.dropdown__q');
  let btnMinus = $(this).parents('.dropdown').find('.dropdown__btn-minus');
  $(this).parents('.dropdown').find('.dropdown__input')[0].value = 'Сколько гостей';
  for (let i = 0; i < 3; i++) {
    quantity[i].value = 0;
    btnMinus[i].setAttribute('disabled', 'disabled');
  }
  $(this).parents('.dropdown').find('.dropdown__clear')[0].style.visibility = 'hidden';
};

let show = function(){
    let parentEl = $(this).parents('.dropdown');
  if (parentEl.find('.dropdown__window')[0].style.display === 'none') {
    parentEl.find('.dropdown__window')[0].style.display = 'block';
    parentEl.find('.dropdown__input')[0].style.borderRadius = '2px 2px 0 0';
  } else {
    parentEl.find('.dropdown__window')[0].style.display = 'none';
    parentEl.find('.dropdown__input')[0].style.borderRadius = '2px 2px 2px 2px';
  }
};

$('.dropdown__input').on('click', show);
$('.dropdown__submit').on('click', result);
$('.dropdown__submit').on('click', show);
$('.dropdown__clear').on('click', cleaning);
$('.dropdown__btn-minus').on('click', minus);
$('.dropdown__btn-plus').on('click', plus);
