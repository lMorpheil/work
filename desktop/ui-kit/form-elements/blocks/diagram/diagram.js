  let canvas=document.getElementById('cnvs');
  let ctx = canvas.getContext('2d');
  // *** First Line
  ctx.beginPath();
  let gradient1 = ctx.createLinearGradient(0, 60, 60, 60);
  gradient1.addColorStop(0, '#FFE39C');
  gradient1.addColorStop(1, '#FFBA9C');
  ctx.lineWidth = 3;
  ctx.translate(60,60);
  ctx.rotate(90 * Math.PI / 180);
  ctx.arc(0, 0,58,0, Math.PI,false);
  ctx.strokeStyle = gradient1;
  ctx.stroke();
  // *** Second Line
  ctx.beginPath();
  let gradient2 = ctx.createLinearGradient(30, -30, 60, 30);
  gradient2.addColorStop(0, '#BC9CFF');
  gradient2.addColorStop(1, '#8BA4F9');
  ctx.lineWidth = 3;
  ctx.rotate(90 * Math.PI / 180);
  ctx.arc(0,0,58,1.6, Math.PI,false);
  ctx.strokeStyle = gradient2;
  ctx.stroke();
  // *** Third Line
  ctx.beginPath();
  let gradient3 = ctx.createLinearGradient(0, 0, 30, 30);
  gradient3.addColorStop(0, '#66D2EA');
  gradient3.addColorStop(1, '#6FCF97');
  ctx.lineWidth = 3;
  ctx.rotate(88 * Math.PI / 180);
  ctx.arc(0,0,58,1.6, Math.PI,false);
  ctx.strokeStyle = gradient3;
  ctx.stroke();
