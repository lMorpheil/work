const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
const merge = require('webpack-merge');
const pug = require('./webpack/pug');
const devserver = require('./webpack/devserver');
const scss = require('./webpack/scss');
const file = require('./webpack/file');

const PATHS = {
  src: path.join(__dirname, 'src'),
  dist: path.join(__dirname, 'dist')
};

const common = merge([
  {
    entry: {
      'index': PATHS.src + '/js/index.js',
      'colorstypes': PATHS.src + '/js/colorstypes.js',
      'cards': PATHS.src + '/js/cards.js',
      'headers-and-footers': PATHS.src + '/js/headers-and-footers.js',
      'landing': PATHS.src + '/js/landing.js',
      'search-room': PATHS.src + '/js/search-room.js',
      'registration': PATHS.src + '/js/registration.js',
      'sign-in': PATHS.src + '/js/sign-in.js',
      'room-details': PATHS.src + '/js/room-details.js'
    },
    output: {
      path: PATHS.dist,
      filename: '[name].js'
    },
    plugins: [
      new HtmlWebpackPlugin({
            filename: 'index.html',
            template: PATHS.src + '/pug/index.pug',
            chunks: ['index']
          }),
      new HtmlWebpackPlugin({
            filename: 'colorstypes.html',
            template: PATHS.src + '/pug/ui-kit/colorstypes.pug',
            chunks: ['colorstypes']
          }),
      new HtmlWebpackPlugin({
            filename: 'cards.html',
            template: PATHS.src + '/pug/ui-kit/cards.pug',
            chunks: ['cards']
          }),
      new HtmlWebpackPlugin({
            filename: 'headers-and-footers.html',
            template: PATHS.src + '/pug/ui-kit/headers-and-footers.pug',
            chunks: ['headers-and-footers']
          }),
      new HtmlWebpackPlugin({
            filename: 'landing.html',
            template: PATHS.src + '/pug/pages/landing.pug',
            chunks: ['landing']
          }),
      new HtmlWebpackPlugin({
            filename: 'search-room.html',
            template: PATHS.src + '/pug/pages/search-room.pug',
            chunks: ['search-room']
          }),
      new HtmlWebpackPlugin({
            filename: 'registration.html',
            template: PATHS.src + '/pug/pages/registration.pug',
            chunks: ['registration']
          }),
      new HtmlWebpackPlugin({
            filename: 'sign-in.html',
            template: PATHS.src + '/pug/pages/sign-in.pug',
            chunks: ['sign-in']
          }),
      new HtmlWebpackPlugin({
            filename: 'room-details.html',
            template: PATHS.src + '/pug/pages/room-details.pug',
            chunks: ['room-details']
          }),
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery'
          })
        ],
      devtool: 'inline-source-map',
      stats: 'errors-only'
  },
  pug(),
  file()
]);


module.exports = function(env) {
  if (env === 'production') {
    return merge([
      common,
      scss()
    ]);
  }
  if (env === 'development') {
    return merge([
      common,
      devserver(),
      scss()
    ]);
  }
}
