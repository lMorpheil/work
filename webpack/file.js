module.exports = function () {
  return {
    module: {
      rules: [
          {
          test: /\.(png|jpe?g|gif|svg)$/i,
          use: [
            {
              loader: 'file-loader',
              options: {
                esModule: false,
                name: 'img/[name].[ext]'
              }
            }
          ]
        }
      ]
    }
  }
};
